module LabelsPlugin
  def has_state_label?
    labels.grep(/^state::.*/).any?
  end

  def has_merge_requests_open?
    mrs.any? { |mr| mr.state == "opened"}
  end

  def has_issues_to_move_to_review?
    closes_issues.map{ |issue| issue.labels.select{ |l| l.name == "state::review"}.length == 0}.flatten.any?
  end

  def closing_merge_request_refs_list
    references = mrs.map { |mr| "[#{mr.resource[:title]}](#{mr.resource[:references][:full]})" }.join("- \n")
    " - #{references}"
  end

  def merge_request_reviewers
      resource[:reviewers].map { |r| "@#{r[:username]}" }.to_sentence
  end

  def closing_issues_refs_list
    references = closes_issues.map { |issue| "[#{issue.resource[:title]}](#{issue.resource[:web_url]})" }.join("- \n")
    " - #{references}"
  end

  def labels
    resource[:labels]
  end

  def comment_found(message)

  end

  def mrs
    closed_by
  end
end

Gitlab::Triage::Resource::Context.include LabelsPlugin