# Triage roject for MitupBot Group

This project contains the configuration to use the [gitlab-triage](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage) tool running in scheduled pipelines execution.

The [.gitlab-ci.yml](.gitlab-ci.yml) contains the pipeline configuration to run triaging while the [.triage-policies.yml](.triage-policies.yml) contains the rules to triage GitLab types.

## Writing rules

Rules can be writen by anyone contributing to the project. They become effective once the changes are merged to `main` after a MR has been approved.

To write a new rule make sure to follow the [README in the gitlab-triage project](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage). 